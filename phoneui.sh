set -euo pipefail
contacts="$(termux-contact-list)"
names=$(echo $contacts | jq ".[].name" | paste -sd, | tr -d '"')
name=$(termux-dialog sheet -t "Liste des contacts" -v "$names" | jq -r ".text")
if [[ -z "$name" ]]; then
	exit 0
fi
number=$(echo $contacts | jq -r ".[] | select(.name == \"$name\") | .number" | tr -d ' ')
action=$(termux-dialog sheet -v "$name ($number),Envoyer un message,Appeler" | jq -r ".text")
if  [[ "$action" == "Appeler" ]]; then
	termux-telephony-call "$number"
fi
if [[ "$action" == "Envoyer un message" ]]; then
	message=$(termux-dialog text -mt "Message à $name" -i "Saisir un message..." | jq -r ".text")
	if [[ ! -z "$message" ]]; then
		termux-sms-send -n "$number" "$message"
	fi
fi
